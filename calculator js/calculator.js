const prompt = require('prompt-sync')();

function tambah(angka1, angka2) {
      return (angka1 + angka2);
}
function kurang(angka1, angka2) {
      return (angka1 - angka2);
}
function kali(angka1, angka2) {
      return (angka1 * angka2);
}
function bagi(angka1, angka2) {
      return (angka1 / angka2);
}

let status = true
while(status){
      let angka1 = parseInt(prompt("Masukkan angka pertama: "));
      let angka2 = parseInt(prompt("Masukkan angka kedua: "));
      
      var operasi = prompt("Pilih operasi ? (tambah/kurang/kali/bagi) : ")
      
          switch (operasi) {
            case 'tambah':
                  console.log("Hasil tambah : ", tambah(angka1 , angka2));
                  break;
      
            case 'kurang':
                  console.log("Hasil kurang : ", kurang(angka1 , angka2));
                  break;
      
            case 'kali':
                  console.log("Hasil kali : ", kali(angka1 , angka2));
                  break;
      
            case 'bagi':
                  console.log("Hasil bagi : ", bagi(angka1 , angka2));
                  break;
            default:
                  console.log("Operasi yang dipilih tidak tersedia!!!");
           }    

      var temp = prompt("Ulangi perhitungan ? (yes/no) : ")
      status = temp == 'yes' ? true : false;
}
