// inisiasi array pertama
const team1 = ["Arsenal", "Everton", "Leicester"];
// inisiasi array kedua
const team2 = ["Liverpool", "MU"];
// menggabukan array pertama dan kedua menggunakan concat()
const teams = team2.concat(team1);
// menampilkan hasil penggabungan array
console.log(teams);