// inisiasi array
const numbers = [2,4,6,7,9,13,14,16,17];

// melakukan filter array number menggunakan filter() lalu disimpan pada mutatedItems
const mutatedItems = numbers.filter(function(item){
    return item % 2;
});

// menampilkan mutatedItems
console.log(mutatedItems);