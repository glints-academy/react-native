// inisiasi array
const numbers = [1,2,3,4,5];

// mengelola data yang berada dalam array menggunakan object.map() 
// lalu disimpan pada variabel mutatedItems
const mutatedItems = numbers.map(function(item){
    return item * 2;
});

// menampilkan hasil maping
console.log(mutatedItems);