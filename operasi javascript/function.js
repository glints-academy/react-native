const phi = 3.14;

// fungsi hitungKeliling membutuhkan parameter r sebagai data yang akan diolah 
// menggunakan fungsi normal
function hitungKeliling1(r){
    return 2 * phi * r;
}
// fungsi hitungLuas membutuhkan parameter r sebagai data yang akan diolah
// menggunakan fungsi normal
function hitungLuas1(r){
    return phi * r * r;
}

// fungsi hitungKeliling membutuhkan parameter r sebagai data yang akan diolah 
// menggunakan fungsi normal
const hitungKeliling2 = function(r){
    return 2 * phi * r;
}
// fungsi hitungLuas membutuhkan parameter r sebagai data yang akan diolah
// menggunakan fungsi normal
const hitungLuas2 = function(r){
    return phi * r * r;
}
  
// fungsi hitungKeliling membutuhkan parameter r sebagai data yang akan diolah 
// menggunakan fungsi inline
const hitungKeliling3 = (r) => 2 * phi * r;
// fungsi hitungLuas membutuhkan parameter r sebagai data yang akan diolah
// menggunakan fungsi inline
const hitungLuas3 = (r) => phi * r * r;

// menggunakan fungsi yang telah dibuat
console.log("Keliling lingkaran declaration :", hitungKeliling1(10));
console.log("Luas lingkaran declaration :", hitungLuas1(10));

console.log("Keliling lingkaran expression :", hitungKeliling2(20));
console.log("Luas lingkaran expression :", hitungLuas2(20));

console.log("Keliling lingkaran arrow :", hitungKeliling3(30));
console.log("Luas lingkaran arrow :", hitungLuas3(30));

// mendapatkan data inputan dari terminal
const input = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

// iterasi inputan dari user
input.question('Masukkan jari-jari lingkaran?', jari => {
    console.log("Keliling lingkaran declaration :", hitungKeliling1(jari));
    console.log("Luas lingkaran declaration :", hitungLuas1(jari));

    console.log("Keliling lingkaran expression :", hitungKeliling2(jari));
    console.log("Luas lingkaran expression :", hitungLuas2(20));

    console.log("Keliling lingkaran arrow :", hitungKeliling3(jari));
    console.log("Luas lingkaran arrow :", hitungLuas3(jari));
    input.close();
});