// Shift menghapus didepan
const teamShift = ["Arsenal", "Everton", "Leicester"];
teamShift.shift();
console.log(teamShift);

// Unsift menambahkan didepan
const teamUnshift = ["Arsenal", "Everton", "Leicester"];
teamUnshift.unshift("Liverpool", "MU");
console.log(teamUnshift);