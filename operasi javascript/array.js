// Struktur array inline
let namaKeluarga = ["Anggia", "Anggria", "Anggara", "Agiel", "Anggini"];

// Array multiple line
let namaKartuKeluarga = [
    "Pak Azis Suyuti", 
    "Bu Almi Asnani",
    "Nak Anggia",
    "Nak Anggria",
    "Nak Anggara",
    "Nak Agiel",
    "Nak Anggini"
];

// create new array from class
let namaKK = new Array("Anggia", "Anggria", "Anggara", "Agiel", "Anggini");

// menampilkan array namaKeluarga
console.log(namaKeluarga);

// menampilkan panjang array namaKartuKeluarga
console.log(namaKartuKeluarga.length);

// menggabungkan dua array ke dalam sebuah array
const arrays = [namaKeluarga, namaKartuKeluarga];
// menampilkan hasil gabungan
console.log(arrays);