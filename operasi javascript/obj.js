// Struktur objek
let ktp = {
    // nama adalah atribut, sedahkan "Agiel Nugraha" adalah value
    nama : "Agiel Nugraha",
    alamat : {
        jalan : "Jl. Bangau",
        "rt/rw" : "003/003",
    },
    // nilai atribur berupa array
    pekerjaan : [
        "Freelancer", "Student"
    ]
}

// menampilkan nilai object ktp
console.log(ktp);

// menampilkan nilai object ktp -> pekerjaan -> jalan
console.log(ktp["alamat"].jalan);