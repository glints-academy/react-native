const axios = require('axios');

// Async await
async function getUser() {
    try {
      const response = await axios.get('http://dummy.restapiexample.com/api/v1/employees');
    //   console.log(response.data);

      return response.data;
    } catch (error) {
        if(error.response.status == 429){
            console.error('Mohon tunggu sebentar')
            console.error(error.message);
        }else{
            console.error("Status : ", error.response.status);
            console.error("Message : ", error.message);
        }
    }
}

async function panggil(){
    let myJSON = await getUser();
    console.log("Data : ", myJSON); 
}


panggil();