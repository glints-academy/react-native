// Syncronous
console.log('First');
console.log('Second');
console.log('Third');

// Asyncronous
console.log('First');
setTimeout(function () {
    console.log('Second');
},0);
console.log('Third');