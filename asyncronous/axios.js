const axios = require('axios');

// Async await
async function getUser() {
    try {
      const response = await axios.get('http://dummy.restapiexample.com/api/v1/employees');
      console.log(response.data);
    } catch (error) {
        if(error.response.status == 429){
            console.error('Mohon tunggu sebentar')
            console.error(error.message);
        }else{
            console.error("Status : ", error.response.status);
            console.error("Message : ", error.message);
        }
    }
}
getUser()

// Implement promise
axios.get('http://dummy.restapiexample.com/api/v1/employees')
.then( res => {
    console.info(res.data);
    console.info(res.status);
    console.info(res.statusText);
    console.info(res.headers);
    console.info(res.config);
}).catch( err => {
    if(err.response.status == 429){
        console.error(err.message);
    }else{
        console.error("Status : ", err.response.status);
        console.error("Message : ", err.message);
    }
});