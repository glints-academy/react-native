// Callback
const UserProfile = function(id, callback) { // callback atau cb
    if(!id) {
        return callback(new Error('Invalid userId')); // syntax to create error
    }
    let result = {
        success : true,
        id : id,
        message : "User Found"
    }
    return callback(null, result);
}

UserProfile(10, function(err, result) {
    if(err) {
        console.log(err.message);
    }
    console.log(result); // guard clause
})