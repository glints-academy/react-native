const requestPromise = require('request-promise');

const options = {
    url : 'https://api.github.com/repos/request/request',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
};

// Promise
requestPromise(options)
.then(function(response) {
    console.log(response.name);
    console.log(response.stargazers_count + " Stars");
    console.log(response.forks_count + " Forks");
})
.catch(function (err) {
    console.log(err)
    console.log("Data not found")
});