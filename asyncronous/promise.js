console.info('PROMISE');

const promiseJalanBareng = new Promise(function(resolve, reject) {
    setTimeout(() => {
        if(true){
            resolve("Oke jalan");
        }else{
            reject(new Error("Sorry, ada acara mendadak"));
        }
    }, 2000);
});

const promiseMakanBareng = new Promise(function(resolve, reject) {
    setTimeout(() => {
        if(false){
            resolve("Oke makan");
        }else{
            reject(new Error("Sorry, lagi sakit gigi"));
        }
    }, 3000);
});

function tryDoPromise() {
    console.info("#tryDoPromise");
    const res = promiseJalanBareng;
    console.info('#tryDoPromise - res', res);
}

function doPromiseUsingThen() {
    console.info("#doPromiseUsingThen");
    promiseJalanBareng.then( res => {
        console.info('#doPromiseUsingThen - res', res);
    }).catch( err => {
        console.error( err.message )
    });
}

function doPromiseUsingMultipleAsync() {
    console.info("#doPromiseUsingMultipleAsync");
    promiseMakanBareng.then( res => {
        console.info('#promiseMakanBareng - res', res);
    }).catch( err => {
        console.error( err.message )
    });

    promiseJalanBareng.then( res => {
        console.info('#promiseJalanBareng - res', res);
    }).catch( err => {
        console.error( err.message )
    });
}

function doPromiseUsingMultipleThen() {
    console.info("#doPromiseUsingMultipleThen");
    promiseJalanBareng.then( res => {
        console.info('#promiseJalanBareng - res', res);
        return promiseMakanBareng;
    }).then( res1 => {
        console.info('#promiseMakanBareng - res1', res);
    }).catch( err => {
        console.error( err.message )
    });
}

function doPromiseUsingMultipleThen2() {
    console.info("#doPromiseUsingMultipleThen");
    promiseJalanBareng.then( res => {
        console.info('#promiseJalanBareng - res', res);

        promiseMakanBareng.then( res1 => {
            console.info('#promiseMakanBareng - res1', res1);
        }).catch( err => {
            console.error( err.message )
        });
    }).catch( err => {
        console.error( err.message )
    });
}

async function  doAsyncAwait() {
    console.info('#doAsyncAwait');
    const resAwait = await promiseJalanBareng;
    console.info('#doAsyncAwait - res: ', resAwait);
}

async function  doAsyncAwaitTryCatch() {
    try {
        console.info('#doAsyncAwaitTryCatch');
        const resAwait = await promiseJalanBareng;
        console.info('#doAsyncAwaitTryCatch - res: ', resAwait);    
        const resAwait1 = await promiseMakanBareng;
        console.info('#doAsyncAwait - res: ', resAwait);
    } catch(err) {
        console.error(err.message)
    }
}

tryDoPromise()
doPromiseUsingThen();
doPromiseUsingMultipleAsync();
doPromiseUsingMultipleThen();
doPromiseUsingMultipleThen2();
doAsyncAwait();
doAsyncAwaitTryCatch();