const requestPromise = require('request-promise');

const options = {
    url : 'https://api.github.com/repos/request/request',
    headers: {
        'User-Agent': 'Request-Promise'
    },
    json: true // Automatically parses the JSON string in the response
};

// Async
async function getData(){
    try {
        const response = await requestPromise(options)
        return response;
    } catch (error) {
        console.log("Data not found");
    }
}

const data = getData();
console.info("Ini data dari getData() : ", data);