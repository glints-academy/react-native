# React Native Learning Documentation

## WEEK 1
#### [HTML CSS](https://gitlab.com/glints-academy/react-native/-/tree/master/html%20css)
- Simple HTML page with styling css

## WEEK 2
#### [Operasi Javascript](https://gitlab.com/glints-academy/react-native/-/tree/master/operasi%20javascript)
- Mengekplorasi macam-macam penulisan fungsi
- Penggunaan fungsi array seperti maping, filter, concat, dll
- Penulisan objek
#### [OOP (Object Oriented Programming)](https://gitlab.com/glints-academy/react-native/-/tree/master/oop)
- Inheritance
- Polymorphism
- Encapsulation
- Abstract
#### [Flowchart](https://gitlab.com/glints-academy/react-native/-/tree/master/flowchart)
- Mengenali simbol-simbol flowchart beserta fungsinya
#### [Calculator JS](https://gitlab.com/glints-academy/react-native/-/tree/master/calculator%20js)
- Membuat fungsi-fungsi perhitungan di javascript
#### [Calculator HTML](https://gitlab.com/glints-academy/react-native/-/tree/master/calculator%20html)
- Membuat calculator melalui tampilan HTML

## WEEK 3
#### [Asynchronous](https://gitlab.com/glints-academy/react-native/-/tree/master/asyncronous)
- Mengenal Callback
- Perbedaan Promise dan Callback
- Penggunakan Async Await
#### [Axios](https://gitlab.com/glints-academy/react-native/-/tree/master/axios%20to%20html)
- Menerapkan konsep promise pada menggunakan axios
- Menerapkan konsep async await pada menggunakan axios
#### [Export Import](https://gitlab.com/glints-academy/react-native/-/tree/master/export%20import)
- Melakukan export import file
- Menerapkan export import pada calcultor
#### [Review OOP](https://gitlab.com/glints-academy/react-native/-/tree/master/review%20oop)
- Melakukan review konsep OOP
- Membuat class berdasarkan slide challenge
####  [React JS News App](https://gitlab.com/daengagiel17/news-app)
- Membuat aplikasi news app menggunakan react js

## WEEK 4
####  [React Native News App](https://gitlab.com/daengagiel17/news-apps)
- Membuat aplikasi news app menggunakan react native
####  [Component, Promise-Axios](https://gitlab.com/daengagiel17/carousel)
- Implementasi dan konsep component
- Implementasi promise dan axios pada react native
####  [Navigation](https://gitlab.com/daengagiel17/carousel)
- Implementasi dan konsep navigation menggunakan modul 'react-native-navigation'

## WEEK 5
####  [Redux](https://gitlab.com/daengagiel17/carousel)
- Implementasi dan konsep redux

## WEEK 6
####  [Auth](https://gitlab.com/daengagiel17/carousel/-/tree/week6)
- Konsep redux saga dan API
- Implementasi pada autentikasi
####  [Content Interest](https://gitlab.com/daengagiel17/carousel/-/tree/week6)
- Challenge menampilkan content dan interest dari API

#### Happy learning, hope it is useful